CC_SHARED = gcc $(CFLAGS) -shared -fPIC
SLANG_INC = -I/usr/local/include
SLANG_LIB = 

LIBS = $(SLANG_LIB) -ltcl8.3 -lm
INCS = $(SLANG_INC)

all: tcl-module.so

tcl-module.so: tcl-module.c
	$(CC_SHARED) $(INCS) tcl-module.c -o tcl-module.so $(LIBS)


clean:
	rm -f tcl-module.so  *.o
