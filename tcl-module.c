#include <stdio.h>
#include <slang.h>
#include <tcl8.3/tcl.h>
SLANG_MODULE(tcl);

#define MODULE_MAJOR_VERSION	0
#define MODULE_MINOR_VERSION	0
#define MODULE_PATCH_LEVEL	0
static char *Module_Version_String = "0.0.0";
#define MODULE_VERSION_NUMBER	\
   (MODULE_MAJOR_VERSION*10000+MODULE_MINOR_VERSION*100+MODULE_PATCH_LEVEL)

/* Define intrinsics here */
Tcl_Interp *interp = NULL;
static int Tcl_Error = 0;

static void sltcl(char *cmd)
{
   char *str;
   if (Tcl_Eval(interp, cmd) != TCL_OK)
     SLang_verror(Tcl_Error, "TCL error: %s", Tcl_GetStringResult(interp));
   if (NULL == (str = SLang_create_slstring(Tcl_GetStringResult(interp))))
     (void)SLang_push_null();
   else
     (void) SLang_push_string(str);
}

#define I SLANG_INT_TYPE
#define V SLANG_VOID_TYPE
#define S SLANG_STRING_TYPE

static SLang_Intrin_Fun_Type Module_Intrinsics [] =
{
   MAKE_INTRINSIC_S("tcl", sltcl, V),
   SLANG_END_INTRIN_FUN_TABLE
};

#undef I
#undef V
#undef S

static SLang_Intrin_Var_Type Module_Variables [] =
{
   MAKE_VARIABLE("_tcl_module_version_string", &Module_Version_String, SLANG_STRING_TYPE, 1),
   SLANG_END_INTRIN_VAR_TABLE
};

static SLang_IConstant_Type Module_Constants [] =
{
   MAKE_ICONSTANT("_tcl_module_version", MODULE_VERSION_NUMBER),
   SLANG_END_ICONST_TABLE
};

static int sltcl_init()
{
   if (NULL == (interp = Tcl_CreateInterp())
       || TCL_ERROR == Tcl_Init(interp))
     {
        return -1;
     }
   (void) Tcl_DeleteCommand(interp, "exit");
   return 0;
}

int init_tcl_module_ns (char *ns_name)
{
   SLang_NameSpace_Type *ns = SLns_create_namespace (ns_name);
   if (ns == NULL)
     return -1;
   if (NULL == interp && -1 == sltcl_init())
     return -1;
   if (Tcl_Error == 0)
     {
	if (-1 == (Tcl_Error = SLerr_new_exception (SL_RunTime_Error, "TclError", "TCL error")))
	  return -1;
     }
   
   if ((-1 == SLns_add_intrin_fun_table (ns, Module_Intrinsics, NULL))
       || (-1 == SLns_add_intrin_var_table (ns, Module_Variables, NULL))
       || (-1 == SLns_add_iconstant_table (ns, Module_Constants, NULL)))
     return -1;

   return 0;
}

